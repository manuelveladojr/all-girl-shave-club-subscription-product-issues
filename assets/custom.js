$(document).ready(function() {
  $("#select-collection").change(function() {
    window.location.href = $(this).val();
  });

  $(".testimonials-section").slick({
    prevArrow:
      '<button type="button" class="slick-prev"><i class="fal fa-chevron-left"></i></button>',
    nextArrow:
      '<button type="button" class="slick-next"><i class="fal fa-chevron-right"></i></button>',
    adaptiveHeight: true
  });

  $(".tooltip").tooltipster({
    theme: "tooltipster-shadow",
    maxWidth: 260,
    interactive: true,
    side: "right"
  });

  // Toggle scent selector
  $('input[name="addon-1"]').change(function() {
    if ($(this).prop("id") !== "addon-1-no-thanks") {
      $(".select-scent").show();
    } else {
      $(".select-scent").hide();
    }
  });

  // Add handle color to review
  $('input[name="handle-color"]').change(function() {
    $('.summary tr[data-type="handle-color"] td:last-child')
      .data("added", true)
      .text($('input[name="handle-color"]:checked').val());
    checkReview();
    $(".goToFrequency").trigger("click");
  });

  // Add frequency to review
  $('input[name="frequency"]').change(function() {
    $('.summary tr[data-type="frequency"] td:last-child')
      .data("added", true)
      .html(
        "Every <span>" +
          $('input[name="frequency"]:checked').val() +
          "</span> months"
      );
    checkReview();
    $(".goToAddons").trigger("click");
  });

  // Add Addon #1 to review
  $('input[name="addon-1"]').change(function() {
    $('.summary tr[data-type="addon-1"] td:last-child').text(
      $('input[name="addon-1"]:checked').val()
    );
    recalculatePricing();
    if ($('input[name="addon-1"]:checked').index() == 4) {
      $('.summary tr[data-type="scent"]').hide();
    } else {
      $('.summary tr[data-type="scent"]').show();
    }
  });

  // Add Scent to review
  $('input[name="scent"]').change(function() {
    $('.summary tr[data-type="scent"] td:last-child').text(
      $('input[name="scent"]:checked').val()
    );
  });

  // Add Addon #2 to review
  $('input[name="addon-2"]').change(function() {
    $('.summary tr[data-type="addon-2"] td:last-child').text(
      $('input[name="addon-2"]:checked').val()
    );
    recalculatePricing();
  });

  // Subscription add to cart workflow
  $(".subscription-steps #add-to-cart-button").click(function() {
    addBaseItemToCart(
      $('input[name="handle-color"]:checked').data("var"),
      1,
      $('input[name="frequency"]:checked').val(),
      "Months",
      "191600"
    );
  });
});

checkReview = function() {
  if (
    $('.summary tr[data-type="handle-color"] td:last-child').data("added") &&
    $('.summary tr[data-type="frequency"] td:last-child').data("added")
  ) {
    $("#add-to-cart-button").prop("disabled", false);
  }
};

$('.subscription-steps a[href^="#"]').click(function() {
  const link = $(this).text();
  let target = $(this.hash);
  if (target.length == 0) {
    target = $('a[name="' + this.hash.substr(1) + '"]');
  }
  if (target.length == 0) {
    target = $("html");
  }
  $("html, body").animate({ scrollTop: target.offset().top - 180 }, 600);
  return false;
});

recalculatePricing = function() {
  var addon1_sub = false;
  if($('input[name="addon-1"]').length > 0) addon1_sub = $('input[name="addon-1"]:checked').data("sub");
  var addon2_sub = false;
  if($('input[name="addon-2"]').length > 0) addon2_sub = $('input[name="addon-2"]:checked').data("sub");
  var base = parseFloat($(".step-1").data("base-price"));
  var addon1 = 0;
  if($('input[name="addon-1"]').length > 0) addon1 = parseFloat($('input[name="addon-1"]:checked').data("cost"));
  var addon2 = 0;
  if($('input[name="addon-2"]').length > 0) addon2 = parseFloat($('input[name="addon-2"]:checked').data("cost"));
  var total = base + addon1 + addon2;
  $('.summary tr[data-type="total"] td:last-child').text(
    "$" + total.toFixed(2).replace(".00", "")
  );
  var recurring = base;
  if (addon1_sub && addon2_sub) {
    recurring = base + addon1 + addon2;
    $('.summary tr[data-type="recurring"] td:last-child').text(
      "$" + recurring.toFixed(2).replace(".00", "")
    );
  }
  if (addon1_sub == true && addon2_sub == false) {
    recurring = base + addon1;
    $('.summary tr[data-type="recurring"] td:last-child').text(
      "$" + recurring.toFixed(2).replace(".00", "")
    );
  }
  if (addon1_sub == false && addon2_sub == true) {
    recurring = base + addon2;
    $('.summary tr[data-type="recurring"] td:last-child').text(
      "$" + recurring.toFixed(2).replace(".00", "")
    );
  }
  if (addon1_sub == false && addon2_sub == false) {
    recurring = base;
    $('.summary tr[data-type="recurring"] td:last-child').text(
      "$" + recurring.toFixed(2).replace(".00", "")
    );
  }
};

function addBaseItemToCart(
  variant_id,
  quantity,
  shipping_interval_frequency,
  shipping_interval_unit_type,
  subscription_id
) {
  data = {
    quantity: quantity,
    id: variant_id,
    "properties[shipping_interval_frequency]": shipping_interval_frequency,
    "properties[shipping_interval_unit_type]": shipping_interval_unit_type,
    "properties[subscription_id]": subscription_id
  };
  jQuery.ajax({
    type: "POST",
    url: "/cart/add.js",
    data: data,
    dataType: "json",
    success: function() {
      if($('input[name="addon-1"]').length > 0) {
        addAddon1();
      } else if($('input[name="addon-2"]').length > 0) {
        addAddon2();
      }
    }
  });
}

function addAddon1ToCart(
  variant_id,
  quantity,
  shipping_interval_frequency,
  shipping_interval_unit_type,
  subscription_id
) {
  data = {
    quantity: quantity,
    id: variant_id,
    "properties[shipping_interval_frequency]": shipping_interval_frequency,
    "properties[shipping_interval_unit_type]": shipping_interval_unit_type,
    "properties[subscription_id]": subscription_id
  };
  jQuery.ajax({
    type: "POST",
    url: "/cart/add.js",
    data: data,
    dataType: "json",
    success: function() {
      if($('input[name="addon-2"]').length > 0) {
        addAddon2();
      } else {
        window.location.href = "/cart";
      }
    }
  });
}

function addAddon2ToCart(
  variant_id,
  quantity,
  shipping_interval_frequency,
  shipping_interval_unit_type,
  subscription_id
) {
  data = {
    quantity: quantity,
    id: variant_id,
    "properties[shipping_interval_frequency]": shipping_interval_frequency,
    "properties[shipping_interval_unit_type]": shipping_interval_unit_type,
    "properties[subscription_id]": subscription_id
  };
  jQuery.ajax({
    type: "POST",
    url: "/cart/add.js",
    data: data,
    dataType: "json",
    success: function() {
      window.location.href = "/cart";
    }
  });
}

addAddon1 = function() {
  // 2. Add add-on #1 (if applicable)
  switch ($('input[name="addon-1"]:checked').index()) {
    case 0:
      // Yes, one-time
      var qty = 1;
      var id = $('input[name="scent"]:checked').data("vid");
      data = {
        quantity: qty,
        id: id
      };
      jQuery.ajax({
        type: "POST",
        url: "/cart/add.js",
        data: data,
        dataType: "json",
        success: function() {
          if($('input[name="addon-2"]').length > 0) {
            addAddon2();
          } else {
            window.location.href = "/cart";
          }
        },
        error: function(errorThrown) {
          console.log("Addon 1: " + errorThrown);
        }
      });
      break;
    case 2:
      // Yes, subscription
      addAddon1ToCart(
        $('input[name="scent"]:checked').data("sub-vid"),
        1,
        $('input[name="frequency"]:checked').val(),
        "Months",
        "192334"
      );
      break;
    case 4:
      // No thanks.
      if($('input[name="addon-2"]').length > 0) {
        addAddon2();
      } else {
        window.location.href = "/cart";
      }
      break;
  }
};

addAddon2 = function() {
  // 3. Add add-on #2 (if applicable)
  switch ($('input[name="addon-2"]:checked').index()) {
    case 0:
      // Yes, one-time
      var qty = 1;
      var id = $(".addon-2").data("vid");
      data = {
        quantity: qty,
        id: id
      };
      jQuery.ajax({
        type: "POST",
        url: "/cart/add.js",
        data: data,
        dataType: "json",
        success: function() {
          window.location.href = "/cart";
        },
        error: function(errorThrown) {
          console.log("Addon 2: " + errorThrown);
        }
      });
      break;
    case 2:
      // Yes, subscription
      addAddon2ToCart(
        $(".addon-2").data("sub-vid"),
        1,
        $('input[name="frequency"]:checked').val(),
        "Months",
        "192344"
      );
      break;
    case 4:
      // No thanks.
      window.location.href = "/cart";
      break;
  }
};
